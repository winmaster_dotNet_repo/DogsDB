﻿namespace KatarzynaBialasLab2
{
    partial class Lab2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNameSurname = new System.Windows.Forms.Label();
            this.dataGridViewListOfDogs = new System.Windows.Forms.DataGridView();
            this.buttonShow = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.dataGridViewInfo = new System.Windows.Forms.DataGridView();
            this.labelDogs = new System.Windows.Forms.Label();
            this.labelDescript = new System.Windows.Forms.Label();
            this.buttonAddDescription = new System.Windows.Forms.Button();
            this.buttonShowDescriptions = new System.Windows.Forms.Button();
            this.pictureBoxDog = new System.Windows.Forms.PictureBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonReadFile = new System.Windows.Forms.Button();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.buttonShowDescription = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfDogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDog)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNameSurname
            // 
            this.labelNameSurname.AutoSize = true;
            this.labelNameSurname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.labelNameSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.labelNameSurname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelNameSurname.Location = new System.Drawing.Point(353, 9);
            this.labelNameSurname.Name = "labelNameSurname";
            this.labelNameSurname.Size = new System.Drawing.Size(207, 39);
            this.labelNameSurname.TabIndex = 0;
            this.labelNameSurname.Text = "Psy Rasowe";
            // 
            // dataGridViewListOfDogs
            // 
            this.dataGridViewListOfDogs.BackgroundColor = System.Drawing.Color.Aquamarine;
            this.dataGridViewListOfDogs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListOfDogs.Location = new System.Drawing.Point(231, 91);
            this.dataGridViewListOfDogs.Name = "dataGridViewListOfDogs";
            this.dataGridViewListOfDogs.Size = new System.Drawing.Size(300, 379);
            this.dataGridViewListOfDogs.TabIndex = 1;
            // 
            // buttonShow
            // 
            this.buttonShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonShow.Location = new System.Drawing.Point(29, 6);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(139, 42);
            this.buttonShow.TabIndex = 2;
            this.buttonShow.Text = "Wyświetl";
            this.buttonShow.UseVisualStyleBackColor = false;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonAdd.Location = new System.Drawing.Point(29, 47);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(139, 42);
            this.buttonAdd.TabIndex = 6;
            this.buttonAdd.Text = "Dodaj psa";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // dataGridViewInfo
            // 
            this.dataGridViewInfo.BackgroundColor = System.Drawing.Color.Bisque;
            this.dataGridViewInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInfo.Location = new System.Drawing.Point(537, 104);
            this.dataGridViewInfo.Name = "dataGridViewInfo";
            this.dataGridViewInfo.Size = new System.Drawing.Size(179, 153);
            this.dataGridViewInfo.TabIndex = 10;
            this.dataGridViewInfo.Visible = false;
            // 
            // labelDogs
            // 
            this.labelDogs.AutoSize = true;
            this.labelDogs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.labelDogs.Location = new System.Drawing.Point(231, 62);
            this.labelDogs.Name = "labelDogs";
            this.labelDogs.Size = new System.Drawing.Size(42, 26);
            this.labelDogs.TabIndex = 11;
            this.labelDogs.Text = "Psy";
            // 
            // labelDescript
            // 
            this.labelDescript.AutoSize = true;
            this.labelDescript.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDescript.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.labelDescript.Location = new System.Drawing.Point(537, 75);
            this.labelDescript.Name = "labelDescript";
            this.labelDescript.Size = new System.Drawing.Size(60, 26);
            this.labelDescript.TabIndex = 12;
            this.labelDescript.Text = "Opisy";
            this.labelDescript.Visible = false;
            // 
            // buttonAddDescription
            // 
            this.buttonAddDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonAddDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonAddDescription.Location = new System.Drawing.Point(29, 135);
            this.buttonAddDescription.Name = "buttonAddDescription";
            this.buttonAddDescription.Size = new System.Drawing.Size(139, 42);
            this.buttonAddDescription.TabIndex = 13;
            this.buttonAddDescription.Text = "Dodaj opis";
            this.buttonAddDescription.UseVisualStyleBackColor = false;
            this.buttonAddDescription.Click += new System.EventHandler(this.buttonAddDescription_Click);
            // 
            // buttonShowDescriptions
            // 
            this.buttonShowDescriptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonShowDescriptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonShowDescriptions.Location = new System.Drawing.Point(29, 174);
            this.buttonShowDescriptions.Name = "buttonShowDescriptions";
            this.buttonShowDescriptions.Size = new System.Drawing.Size(139, 42);
            this.buttonShowDescriptions.TabIndex = 17;
            this.buttonShowDescriptions.Text = "Wyświetl opisy";
            this.buttonShowDescriptions.UseVisualStyleBackColor = false;
            this.buttonShowDescriptions.Click += new System.EventHandler(this.buttonShowDescriptionsInDataGridview_Click);
            // 
            // pictureBoxDog
            // 
            this.pictureBoxDog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxDog.InitialImage = null;
            this.pictureBoxDog.Location = new System.Drawing.Point(12, 301);
            this.pictureBoxDog.Name = "pictureBoxDog";
            this.pictureBoxDog.Size = new System.Drawing.Size(180, 184);
            this.pictureBoxDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDog.TabIndex = 16;
            this.pictureBoxDog.TabStop = false;
            this.pictureBoxDog.Visible = false;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonRefresh.Location = new System.Drawing.Point(29, 91);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(139, 42);
            this.buttonRefresh.TabIndex = 18;
            this.buttonRefresh.Text = "Aktualizuj";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonSave.Location = new System.Drawing.Point(29, 215);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(139, 42);
            this.buttonSave.TabIndex = 19;
            this.buttonSave.Text = "Zapisz do pliku";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonReadFile
            // 
            this.buttonReadFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonReadFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonReadFile.Location = new System.Drawing.Point(29, 253);
            this.buttonReadFile.Name = "buttonReadFile";
            this.buttonReadFile.Size = new System.Drawing.Size(139, 42);
            this.buttonReadFile.TabIndex = 20;
            this.buttonReadFile.Text = "Odczyt z pliku";
            this.buttonReadFile.UseVisualStyleBackColor = false;
            this.buttonReadFile.Click += new System.EventHandler(this.buttonReadFile_Click);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(536, 268);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(180, 169);
            this.textBoxDescription.TabIndex = 21;
            this.textBoxDescription.Visible = false;
            // 
            // buttonShowDescription
            // 
            this.buttonShowDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonShowDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonShowDescription.Location = new System.Drawing.Point(556, 443);
            this.buttonShowDescription.Name = "buttonShowDescription";
            this.buttonShowDescription.Size = new System.Drawing.Size(139, 42);
            this.buttonShowDescription.TabIndex = 22;
            this.buttonShowDescription.Text = "pokaż opis";
            this.buttonShowDescription.UseVisualStyleBackColor = false;
            this.buttonShowDescription.Visible = false;
            this.buttonShowDescription.Click += new System.EventHandler(this.buttonShowDescription_Click);
            // 
            // Lab2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::KatarzynaBialasLab2.Properties.Resources.back2;
            this.ClientSize = new System.Drawing.Size(734, 507);
            this.Controls.Add(this.buttonShowDescription);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.buttonReadFile);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.buttonShowDescriptions);
            this.Controls.Add(this.pictureBoxDog);
            this.Controls.Add(this.buttonAddDescription);
            this.Controls.Add(this.labelDescript);
            this.Controls.Add(this.labelDogs);
            this.Controls.Add(this.dataGridViewInfo);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.dataGridViewListOfDogs);
            this.Controls.Add(this.labelNameSurname);
            this.Name = "Lab2";
            this.Text = "Baza";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfDogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNameSurname;
        private System.Windows.Forms.DataGridView dataGridViewListOfDogs;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.DataGridView dataGridViewInfo;
        private System.Windows.Forms.Label labelDogs;
        private System.Windows.Forms.Label labelDescript;
        private System.Windows.Forms.Button buttonAddDescription;
        private System.Windows.Forms.PictureBox pictureBoxDog;
        private System.Windows.Forms.Button buttonShowDescriptions;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonReadFile;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button buttonShowDescription;
    }
}

