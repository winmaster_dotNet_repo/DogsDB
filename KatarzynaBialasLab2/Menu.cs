﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public partial class Lab2 : Form
    {
        #region Inicjalizacja zmiennych globalnych
        List<Dog> listOfDogs=new List<Dog>();
        FormAddInfo formAddInfo;
        FormAddDog formAddDog;
        #endregion

        #region Funkcje do obslugi formularzy(przekazywanie wartosci)
        private string GetFormAddDogTextBoxName()
        {
            return formAddDog.TextBoxName.Text;
        }

        private string GetFormAddDogTextBoxLand()
        {
            return formAddDog.TextBoxLand.Text;
        }

        private string GetFormAddDogTextBoxSize()
        {
            return formAddDog.TextBoxSize.Text;
        }

        private string GetFormAddInfoTextBoxInfo()
        {
            return formAddInfo.TextBoxInfo.Text;
        }
        #endregion

        #region Inicjuje component z danymi wejsciowymi
        public Lab2()
        {
            InitializeComponent();
            listOfDogs.Add(new Dog("Amerykański Terrier", "USA", 4));
            listOfDogs.Add(new Dog("Beagle", "Wielka Brytania", 3));
            listOfDogs.Add(new Dog("Berneński pies pasterski", "Szwajcaria", 5));
            listOfDogs.Add(new Dog("Buldog Angielski", "Wilka Brytania", 3));
            listOfDogs.Add(new Dog("Buldog Francuski", "Francja", 3));
            listOfDogs.Add(new Dog("Bulterier", "Wielka Brytania", 3));
            listOfDogs.Add(new Dog("Cavalier king charles spaniel", "Wielka Brytania", 2));
            listOfDogs.Add(new Dog("Dog Niemiecki", "Niemcy", 5));
            listOfDogs.Add(new Dog("Golden retriever", "Wielka Brytania", 4));
            listOfDogs.Add(new Dog("Grzywacz chiński", "Chiny", 1));
        
            listOfDogs[0].addInfo("Jak na swoje wymiary pies ten powinien sprawiać wrażenie wyjątkowo silnego. Proporcjonalnie zbudowany, dobrze umięśniony, a przy tym elegancki i zwinny. Żywo reaguje na otoczenie. Zwarty, krępy, ale nie wysokonożny, lub lekkiej konstrukcji. ");
            listOfDogs[0].addInfo("American staffordshire terrier jest dynamicznym, odważnym i energicznym zwierzęciem, stąd ma on dużą potrzebę ruchu. Jego żywotność bardzo przydaje się na wsi, gdzie oczyszcza on teren ze szkodników. Ułożenie tego silnego psa polega na podporządkowaniu go przewodnikowi od szczenięcia. Ma on skłonność do bójek ze względu na pierwotne przeznaczenie, toteż szczenięta należy od początku uczyć przyjaznych kontaktów z innymi psami. Amstaffy wymagają bliskiego kontaktu z właścicielem.");
            listOfDogs[1].addInfo("Sądzi się, że beagle powstał w drodze selekcjonowania najmniejszych foxhoundów do polowania z pieszym myśliwym, najczęściej na zająca. Do dzisiaj wykorzystywany jest do polowań w sforach, często organizowanych przez rozmaite stowarzyszenia i instytucje. Jest to mały pies pełen energii, entuzjazmu i wigoru, zawsze gotów do każdego zajęcia. Wszystkie cechy tego psa powinny składać się na obraz sportowca, a nie ma piękniejszego widoku niż sfora beagle gnająca za zdobyczą z głowami przy ziemi i ogonami postawionymi sztywno do góry. W czasie panowania Henryka VIII i Elżbiety I istniały także beagle szorstkowłose, niektóre tak małe, że mieściły się w kieszeni kurty myśliwskiej.");
            listOfDogs[2].addInfo("Berneński Pies Pasterski jest psem wiejskim o starym rodowodzie, wyko¬rzystywanym u podnóża Alp, w części Wyżyny Szwajcarskiej i w okolicach Berna jako pies stróżujący, pociągowy i zaganiający. Jego pierwotna nazwa  Durrbachler pochodzi od zajazdu i gospody Duerrbach koło Riggisbergu, gdzie ten długowłosy, trójbarwny pies podwórzowy był szczególnie chętnie trzymany.");
            listOfDogs[3].addInfo("Krótkowłosy, dość przysadzisty, raczej niskonożny, mocny, o dobrze rozbudowanym, zwartym tułowiu. Głowa stosunkowo duża w proporcji do wielkości psa, ale ani ona, ani żadna inna partia ciała nie zakłócają ogólnej harmonii, nie dają wrażenia deformacji, ani nie wpływają na sprawność ruchu. Kufa stosunkowo krótka, szeroka, tępa i lekko, ale nie przesadnie zadarta. Wszelkie objawy trudności w oddychaniu w najwyższym stopniu niepożądane. Tułów stosunkowo krótki, zwarty, jakakolwiek nadwaga niepożądana. Kończyny mocne, masywne, krzepkie. Zad wysoki i mocny.");
            listOfDogs[4].addInfo("Prawdopodobnie, jak wszystkie dogowate i molosy z Epiru i Cesarstwa Rzymskiego, ten krewniak buldoga angielskiego, psów średniowiecznych Alanów i małych odmian francuskich ras dogowatych, znany dziś jako buldog francuski, jest wynikiem rozmaitych krzyżówek, przeprowadzanych przez zapalonych hodowców w robotniczych dzielnicach Paryża około roku 1880. W okresie tym, buldogi hodowali rzeźnicy i wozacy paryskiego targowiska les Halles; wkrótce jednak, dzięki niezwykłemu wyglądowi i usposobieniu, rasa zjednała sobie względy arystokratów i artystów.");
            listOfDogs[5].addInfo("Ustalenia typu rasowego, charakteryzującego się jajowatą głową, dokonał w latach pięćdziesiątych XIX wieku niejaki James Higgs. Psy takie pokazano po raz pierwszy na wystawie w Birmingham w roku 1862, a klub rasy powstał w roku 1887. Standard bull teriera jest wyjątkowy z tego względu, że nie podaje wzrostu ani wagi psa, stwierdzając jedynie, że ma on być pod każdym względem harmonijny. Od początku XIX wieku znane były także bull teriery w wersji miniaturowej, ale w początkach XX wieku odmiana ta popadła w zapomnienie, a w roku 1918 Kennel Club wykreślił ją z listy uznanych ras. Jej uratowanie zawdzięczamy grupie entuzjastów pod wodzą pułkownika Richarda Glynna, założyciela klubu odmiany miniaturowej (1938). Wzorzec odmiany miniaturowej jest identyczny, podaje tylko limit wzrostu.");
            listOfDogs[6].addInfo("Przodkami tych psów były płochacze francuskie i hiszpańskie. W XVI wieku Cavaliery cenione były we Francji głównie przez Henryka III, a później przez Ludwika XIV. W Anglii znane były w kręgach arystokracji jako psy do towarzystwa. Rasa ta znalazła uznanie wśród takich osobistości jak: Karol I i II, od którego wzięła się nazwa rasy, hodowali je także wcześniej: James I (1437-1460), Henryk VIII, Elżbieta I oraz Maria Stuart oraz Mistrz Michał z Księstwa Lubniewic.");
            listOfDogs[7].addInfo("Szlachetny wygląd doga niemieckiego jest skutkiem połączenia potężnej, silnej i harmonijnej sylwetki z elegancją, dumą i potęgą. Wielkość, połączona ze szlachetnością, harmonijną budową, proporcjonalną sylwetką i pełną wyrazu głową powodują, że dog niemiecki przykuwa uwagę, jak najwyższej klasy rzeźba. To prawdziwy Apollo wśród wszystkich psów. ");
            listOfDogs[8].addInfo("Prawdopodobnie goldeny zostały wyhodowane przez lorda Dudleya Mjoribanksa. W 1858 r. lord miał obejrzeć przedstawienie rosyjskiej grupy cyrkowej, którego główną atrakcją były pokazy kilku owczarków o żółtawej maści. Urzeczony ich umiejętnościami lord odkupił je i przywiózł do swojej posiadłości - i to one miały być przodkami goldenów. Chociaż historię tę często się przytacza gdy mowa jest o początkach rasy, większość kynologów uważa ją za mało prawdopodobną. Owczarki cechują się zupełnie innymi cechami niż psy myśliwskie. Związek z tą opowieścią ma jedynie to, że pierwsze goldeny pojawiły się na wystawie pod nazwą rosyjskich retrieverów.");
            listOfDogs[9].addInfo("Czy naprawdę ma swoje początki w Chinach? Nie wiadomo, a przynajmniej nie dowiedziono tego z całą pewnością. Jednak nie brakuje przesłanek, iż w Państwie Środka żyły bezwłose psy.");
        }
        #endregion

        #region Funckje przyciskow menu
        private void buttonShow_Click(object sender, EventArgs e)
        {
            //przypisanie nulla umowzliwa refresh
            dataGridViewListOfDogs.DataSource = null;
            dataGridViewListOfDogs.DataSource=listOfDogs;
            dataGridViewListOfDogs.BackgroundColor = Color.AliceBlue;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveAndRead sv = new SaveAndRead();
            sv.SaveDataSerializationDog(listOfDogs, "ListOfDogsDatabase.bin");
            MessageBox.Show("Pomyślnie zapisano dane do pliku");
        }

        private void buttonReadFile_Click(object sender, EventArgs e)
        {
            SaveAndRead sv = new SaveAndRead();
            dataGridViewListOfDogs.DataSource = null;
            dataGridViewInfo.DataSource = null;
            dataGridViewListOfDogs.DataSource= sv.ReadDataDeserializationDog("ListOfDogsDatabase.bin");
            listOfDogs = null;
            listOfDogs = sv.ReadDataDeserializationDog("ListOfDogsDatabase.bin");
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            formAddDog = new FormAddDog();
            formAddDog.ShowDialog();
            string name = GetFormAddDogTextBoxName();
            string land = GetFormAddDogTextBoxLand();
            int size = Int32.Parse(GetFormAddDogTextBoxSize());
            listOfDogs.Add(new Dog(name, land, size));
            dataGridViewListOfDogs.Refresh();
        }

        private void buttonAddDescription_Click(object sender, EventArgs e)
        {
            if (dataGridViewListOfDogs.SelectedRows.Count > 0)
            {
                formAddInfo = new FormAddInfo();
                formAddInfo.ShowDialog();
                var name = dataGridViewListOfDogs.SelectedRows[0].Cells[0].Value;
                listOfDogs.Find(dog => dog.Name == name).Informations.Add(new Informations(GetFormAddInfoTextBoxInfo()));
                dataGridViewInfo.DataSource = null;
                dataGridViewInfo.DataSource = listOfDogs.Find(dog => dog.Name == name).Informations;
            }
            else
            {
                MessageBox.Show("Nie zaznaczono pieska");
            }
        }

        private void buttonShowDescriptionsInDataGridview_Click(object sender, EventArgs e)
        {
            dataGridViewInfo.Visible = true;
            labelDescript.Visible = true;
            pictureBoxDog.Visible = true;
            textBoxDescription.Visible = true;
            buttonShowDescription.Visible = true;
            pictureBoxDog.Refresh();
            dataGridViewListOfDogs.DataSource = listOfDogs;

            if (dataGridViewListOfDogs.SelectedRows.Count > 0)
            {
                var name = dataGridViewListOfDogs.SelectedRows[0].Cells[0].Value;
                dataGridViewInfo.DataSource = null;
                List<Informations> sourceInfos = listOfDogs.Find(dog => dog.Name == name).Informations;
                dataGridViewInfo.DataSource = sourceInfos;
                int number = dataGridViewListOfDogs.SelectedRows[0].Index;
                number = number + 1;

                if (number > 10)
                {
                    pictureBoxDog.Image = KatarzynaBialasLab2.Properties.Resources.pieseł;
                }
                else
                {
                    string foto = number.ToString();
                    pictureBoxDog.Image = KatarzynaBialasLab2.Properties.Resources.foto(foto);
                }
            }
            else
            {
                MessageBox.Show("Nie zaznaczono pieska");
            }
        }

        private void buttonShowDescription_Click(object sender, EventArgs e)
        {
            if (dataGridViewInfo.SelectedRows.Count > 0)
            {
                string description = dataGridViewInfo.SelectedRows[0].Cells[0].Value.ToString();
                textBoxDescription.Text = description;
            }
            else
            {
                MessageBox.Show("Nie zaznaczono opisu");
            }
}
        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            //przypisanie nulla umowzliwa refresh
            dataGridViewListOfDogs.DataSource = null;
            dataGridViewListOfDogs.DataSource = listOfDogs;
        }
        #endregion
    }
}
