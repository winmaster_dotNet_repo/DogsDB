﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public class SaveAndRead
    {
        public SaveAndRead()
        {
            
        }

        public void SaveDataSerializationDog(List<Dog> list, string name)
        {
            string file = name;
            //serialize
            using (Stream stream = File.Open(name, FileMode.Create))
            {
                var binarryformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binarryformatter.Serialize(stream, list);
            }
        }
        
        public List<Dog> ReadDataDeserializationDog(string name)
        {
            List<Dog> list;
            //deserialize
            using (Stream stream = File.Open(name, FileMode.Open))
            {
                var binarryformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                list = (List<Dog>)binarryformatter.Deserialize(stream);
            }
            return list;
        }
    }
}
