﻿namespace KatarzynaBialasLab2
{
    partial class FormAddDog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSize = new System.Windows.Forms.Label();
            this.labelLand = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxSize = new System.Windows.Forms.TextBox();
            this.textBoxLand = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelSize.Location = new System.Drawing.Point(40, 117);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(142, 20);
            this.labelSize.TabIndex = 15;
            this.labelSize.Text = "Rozmiar(skala 1-5)";
            // 
            // labelLand
            // 
            this.labelLand.AutoSize = true;
            this.labelLand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelLand.Location = new System.Drawing.Point(40, 73);
            this.labelLand.Name = "labelLand";
            this.labelLand.Size = new System.Drawing.Size(36, 20);
            this.labelLand.TabIndex = 14;
            this.labelLand.Text = "Kraj";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelName.Location = new System.Drawing.Point(39, 27);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(57, 20);
            this.labelName.TabIndex = 13;
            this.labelName.Text = "Nazwa";
            // 
            // textBoxSize
            // 
            this.textBoxSize.Location = new System.Drawing.Point(43, 135);
            this.textBoxSize.Name = "textBoxSize";
            this.textBoxSize.Size = new System.Drawing.Size(100, 20);
            this.textBoxSize.TabIndex = 12;
            // 
            // textBoxLand
            // 
            this.textBoxLand.Location = new System.Drawing.Point(43, 94);
            this.textBoxLand.Name = "textBoxLand";
            this.textBoxLand.Size = new System.Drawing.Size(100, 20);
            this.textBoxLand.TabIndex = 11;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(43, 50);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 10;
            // 
            // buttonOK
            // 
            this.buttonOK.AccessibleRole = System.Windows.Forms.AccessibleRole.Equation;
            this.buttonOK.Location = new System.Drawing.Point(53, 173);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // FormAddDog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(207, 208);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.labelLand);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxSize);
            this.Controls.Add(this.textBoxLand);
            this.Controls.Add(this.textBoxName);
            this.Name = "FormAddDog";
            this.Text = "Dodaj  psa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Label labelLand;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxSize;
        private System.Windows.Forms.TextBox textBoxLand;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonOK;
    }
}